$(document).ready(function() {
	document.body.addEventListener('click', () => {
		if (document.body.classList.contains("mobile-menu-active")) {
			document.body.classList.remove("mobile-menu-active");
		}
	})

	$('.menu-opener').on('click', function(e) { // Наша бургер кнопка на которую мы будем нажимать
		e.preventDefault();
		e.stopPropagation();
		$('body').toggleClass('mobile-menu-active') // body это селектор на который будет довешиваться класс mobile-menu-active при нажатии на бургер кнопку
	})


})